# groupme.dart
##### groupme api wrapper for dart

### Usage

```Dart
import "dart:async";
import "package:groupme/groupme.dart";

Future<dynamic> main() async {
	Client client = new Client("b81e5db0fed001340ba43bdd17caa119");
	print(await client.groups.index());
}
```
