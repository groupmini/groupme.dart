import "dart:async";
import "dart:convert";
import "package:groupme/src/model.dart";
import "package:http/http.dart" show Response;
import "package:http/http.dart" as http;

const base = "https://api.groupme.com/v3/";

/// Contains methods involved with making requests to the Groupme REST API
class Client {
  String token;
  Groups groups;

  Client(String token) {
    this.token = token;
    groups = new Groups(this);
  }

  /// Constructs an http GET request and returns the JSON response as a String to be decoded later
  Future<String> get(String type,
      {Map<String, String> parameters: const {}}) async {
    try {
      String params = _ptos(parameters);
      String url = "${base}${type}?${params.toString()}";
      Response response = await http.get(url);
      return response.body;
    } catch (error) {
      return "Error conducting GET request";
    }
  }

  /// Constructs an http POST request and returns the JSON response as a String to be decoded later
  Future<Response> post(String type, Map<String, String> parameters) {
    String url = "${base}${type}?token=${token}";
    return http.post(url, body: JSON.encode(parameters));
  }

  /// Converts a map of parameters to a URL-friendly string
  ///
  /// For example: { "token" : "123", "abc" : "xyz" } becomes token=123&abc=xyz
  String _ptos(Map<String, String> parameters) {
    StringBuffer params = new StringBuffer();
    params.write("token=${token}");
    if (parameters.length > 0) {
      parameters.forEach((key, value) {
        params.write("&${key}=${value}");
      });
    }
    return params.toString();
  }
}

/// Contains request methods involving the user's groups
class Groups {
  Client client;
  Groups(this.client);

  /// Returns a list of the authenticated user's active groups
  Future<List<Group>> index() async {
    // TODO implement page and perPage parameters
    return _groups("groups");
  }

  /// Returns a list of groups that the authenticated user has left but can still rejoin
  Future<List<Group>> former() async {
    // TODO implement page and perPage parameters
    return _groups("groups/former");
  }

  /// Returns a specific group given the group ID
  Future<Group> show(String id) async {
    return _group(await client.get("groups/${id}"));
  }

  /// Creates a group given a name (and an optional description, image URL, and whether to create a share URL) and returns it
  Future<Group> create(String name,
      {String description: "", String imageURL: "", bool share: false}) async {
    Response response = await client.post("groups", {
      "name": name,
      "description": description,
      "image_url": imageURL,
      "share": share.toString()
    });
    return _group(response.body);
  }

  Future<Messages> messages(String id, {int limit: 20}) async {
    String response = await client.get("groups/${id}/messages", parameters: {
      "limit": "$limit",
    });
    return _messages(response);
  }

  /// Decodes the response body, extracts the "response" portion, reencodes it as a JSON string, and returns a new Group from it
  Group _group(String string) {
    Map decoded = JSON.decode(string);
    Map response = decoded["response"];
    String jsonString = JSON.encode(response);
    return new Group.fromJsonString(jsonString);
  }

  Messages _messages(String string) {
    Map decoded = JSON.decode(string);
    Map response = decoded["response"];
    String jsonString = JSON.encode(response);
    return new Messages.fromJsonString(jsonString);
  }

  Future<List<Group>> _groups(String endpoint) async {
    List<Group> groups = new List<Group>();
    String body = await client.get(endpoint);
    List parsed = JSON.decode(body)["response"];
    parsed.forEach(
        (item) => groups.add(new Group.fromJsonString(JSON.encode(item))));
    return groups;
  }
}
