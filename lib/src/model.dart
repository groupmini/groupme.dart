import "package:json_object/json_object.dart";

abstract class GroupModel {
  String id, name, type, description, image_url, creator_user_id, share_url;
  int created_at, updated_at;
  List<Member> members;
  List<Message> messages;

  // TODO
  // Map messages;
}

abstract class MemberModel {
  String user_id, nickname, image_url;
  bool muted;
}

class Message {
  String id,
      source_guid,
      user_id,
      group_id,
      name,
      avatar_url,
      text,
      attachments;
  int created_at;
  bool system;
  List<String> favorited_by;
}

class MessagesModel {
  int count;
  List<Message> messages;
}

class Group extends JsonObject implements GroupModel {
  Group();

  factory Group.fromJsonString(String string) {
    return new JsonObject.fromJsonString(string, new Group());
  }
}

class Member extends JsonObject implements MemberModel {
  Member();

  factory Member.fromJsonString(String string) {
    return new JsonObject.fromJsonString(string, new Member());
  }
}

class Messages extends JsonObject implements MessagesModel {
  Messages();

  factory Messages.fromJsonString(String string) {
    return new JsonObject.fromJsonString(string, new Messages());
  }
}
